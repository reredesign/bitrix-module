<?php
/**
 * Created by ReRe Design.
 * User: Semyonchick
 * MailTo: semyonchick@gmail.com
 * DateTime: 28.05.13 15:30
 */

class ImportModel
{
    public static function createElement($data, $type, $parent_id = false, $lastMod = false, $clear = false)
    {
        if (count($data)) switch ($type):
            case 'group':
                if (!$id = self::getPage('external_id', $data['external_id']))
                    $id = self::newPage('product', 1, $data, $parent_id);
                if ($id && is_array($data['group']))
                    foreach ($data['group'] as $row)
                        self::createElement($row, $type, $id, $lastMod, $clear);
                return $id;
            case 'prop':
                if (!self::getCharacter($data['external_id']))
                    return self::newCharacter($data);
                return false;
            case 'item':
                if (!$data['external_id']) return false;
                elseif (!$id = self::getPage('external_id', $data['external_id']))
                    return self::newPage('product', 0, self::readyProduct($data));
                return self::editPage($id, $data, $lastMod);
            case 'priceType':
                if (!self::getPriceType($data['external_id']))
                    return self::newPriceType($data);
                return false;
            case 'offer':
                return self::offer($data, $lastMod);
        endswitch;
        return false;
    }

    public static function getPage($name, $value)
    {
        $sql = 'SELECT `page_id` FROM `character_varchar` WHERE `character_id`=:id AND `lang_id`=:lang AND `value`=:value';
        $params = array(
            'id' => Character::idMap($name),
            'lang' => Yii::app()->language,
            'value' => $value,
        );
        return Yii::app()->db->createCommand($sql)->queryScalar($params);
    }

    public static function getCharacter($value)
    {
        $sql = 'SELECT `id` FROM `character` WHERE `external_id`=:id';
        $params = array('id' => $value);
        return Yii::app()->db->createCommand($sql)->queryScalar($params);
    }

    public static function getPriceType($value = false)
    {
        if ($value) {
            $sql = 'SELECT `id` FROM `price_type` WHERE `external_id`=:id';
            $params = array('id' => $value);
            return Yii::app()->db->createCommand($sql)->queryScalar($params);
        } else {
            $sql = 'SELECT `external_id`, `id` FROM `price_type`';
            return Yii::app()->db->createCommand($sql)->queryAll();
        }
    }

    public static function newPage($type, $category, $data, $parent_id = false)
    {
        $model = new PageModule();
        $model->created = new CDbExpression('NOW()');
        $model->module_id = Module::idMap($type);
        $model->parent_id = $parent_id ? $parent_id : $model->getRoot()->id;
        $model->is_category = $category;
        $model->setAttributes($data, false);
        if ($model->parent_id && $model->save(false)) {
            Url::create($model);
            self::newPhoto($data['image'], $model->id);
        }
        $id = $model->id;
        return $id;
    }

    public static function editPage($id, $data, $lastMod)
    {
        $sql = 'SELECT `id` FROM `page` WHERE `id`=:id AND `lastmod`>FROM_UNIXTIME(:lastmod)';
        $params = array('id' => $id, 'lastmod' => $lastMod);
        $data = self::readyProduct($data);
        if (Yii::app()->db->createCommand($sql)->queryScalar($params)) return true;
        $model = PageModule::model()->findByPk($id);
        self::newPhoto($data['image'], $model->id);
        if ($oldData = $model->attributes) $model->setAttributes($data, false);
        if ($model->attributes != $oldData) $result = $model->save(false);
        else $result = Yii::app()->db->createCommand('UPDATE `page` SET `lastmod`=NOW() WHERE `id`=:id')->execute(compact('id'));
        return $result;
    }

    public static function newCharacter($data)
    {
        $model = new Character();
        $model->setAttributes($data, false);
        $model->url = Yii::app()->formatter->tagUrl($model->name) . '1c';
        $model->type = 'varchar';
        $model->inputType = 'text';
        $model->position = 'additional';
        $model->order = 100;
        $model->save(false);
        $model->clearCache();
        $model->unsetAttributes();
        return true;
    }

    public static function newPriceType($data)
    {
        $model = new PriceType();
        $model->setAttributes($data, false);
        if ($model->taxInclude == 'false') $model->taxInclude = 0;
        $model->save(false);
        $model->unsetAttributes();
        return true;
    }

    public static function offer($data, $lastmod = false)
    {
        if (!$id = self::getPage('external_id', $data['external_id'])) return false;
        $sql = 'SELECT `id` FROM `price` WHERE `page_id`=:id AND `lastmod`>FROM_UNIXTIME(:lastmod)';
        if ($lastmod && Yii::app()->db->createCommand($sql)->queryScalar(compact('id', 'lastmod'))) return false;

        $sql = 'SELECT `id`, `type_id` FROM `price` WHERE `page_id`=:id';
        $prices = Yii::app()->db->createCommand($sql)->queryAll(true, compact('id'));
        $prices = CHtml::listData($prices, 'type_id', 'id');

        if (!$priceTypes = Yii::app()->params['saveData.priceTypes'])
            Yii::app()->params['saveData.priceTypes'] = $priceTypes = CHtml::listData(self::getPriceType(), 'external_id', 'id');

        $priceData = array();
        foreach ($data['price'] as $price) {
            $type_id = $priceTypes[$price['external_id']];
            $priceData[] = array(
                'id' => $prices[$type_id],
                'page_id' => $id,
                'type_id' => $type_id,
                'count' => $data['count'] ? $data['count'] : 0,
                'value' => $price['value'],
                'unit' => $price['unit'],
            );
        }
        $update = array('value', 'count', 'lastmod' => 'NOW()');

        return SaveDAO::execute('price', $priceData, $update);
    }

    public static function newPhoto($name, $pageId)
    {
        if ($name && (file_exists($file = Yii::app()->params['parseDir'] . $name) || file_exists($file = Yii::getPathOfAlias('webroot.data._upload1c.base') . DIRECTORY_SEPARATOR . $name)) && is_file($file)) {
            $filename = basename($file);
            $sql = 'SELECT `id` FROM `photo` WHERE `parent_id`=:pageId AND `name`=:filename';
            if (Yii::app()->db->createCommand($sql)->queryScalar(compact('pageId', 'filename'))) return false;
            $target = Yii::getPathOfAlias('webroot.data._tmp') . DIRECTORY_SEPARATOR . $filename;
            if(file_exists($target)) $result = true;
            else $result = Yii::app()->imageConverter->convert($file, $target, '_tmp');
            if ($result) {
                $size = getimagesize($target);
                $sql = 'SELECT `id` FROM `photo` WHERE `parent_id`=:pageId AND `order`=1';
                $data = array(array(
                    'id' => Yii::app()->db->createCommand($sql)->queryScalar(compact('pageId')),
                    'parent_id' => $pageId,
                    'name' => $filename,
                    'width' => $size[0],
                    'height' => $size[1],
                    'cropParams' => 'N;',
                    'order' => '1',
                ));
                @unlink($file);
                return SaveDAO::execute('photo', $data, array('parent_id', 'name', 'width', 'height'));
            } else return false;
        } else return false;
    }

    public static function readyProduct($data)
    {
        $characters = Character::map();
        foreach ($data as $key => $val) {
            if (in_array($key, array('propValue', 'OtherValue', 'tax'))) {
                unset($val);
            } elseif ($key == 'content') {
                $val = CHtml::tag('p', array(), nl2br(trim($val)));
            } elseif ($key == 'parent_id') {
                $val = self::getPage('external_id', $val);
            } elseif ($key == 'propValue') {
                foreach ($val as $row)
                    $data[$characters['url'][array_search($row['id'], $characters['external_id'])]] = $row['value'];
            } elseif ($key == 'likeItem') {
                $result = CHtml::listData($val, 'id', 'id');
                $sql = 'SELECT GROUP_CONCAT(`page_id`) FROM `character_varchar` WHERE `character_id`=:id AND `lang_id`=:lang AND `value` IN ("' . implode('","', $result) . '")';
                $params = array(
                    'id' => $characters['id']['external_id'],
                    'lang' => Yii::app()->language,
                );
                $val = Yii::app()->db->createCommand($sql)->queryScalar($params);
            } elseif (is_string($val)) {
                $val = str_replace(array("\r", "\n"), '', $val);
                $val = preg_replace('/[\s]{2,}/', ' ', $val);
                $val = trim($val);
            } elseif (is_array($val)) $val = current($val);
            if (isset($val)) $data[$key] = $val;
            else unset($data[$key]);
        }
        return $data;
    }

    public static function clearBase($time)
    {
        $sql = 'DELETE FROM `page` WHERE `module_id`=8 AND `is_category`=0 AND `lastmod` < FROM_UNIXTIME(' . $time . ')';
        if (Yii::app()->db->createCommand($sql)->execute()) {
            $photos = Yii::app()->db->createCommand('SELECT `name` FROM `photo`')->queryColumn();
            $dir = Yii::getPathOfAlias('webroot.data._tmp') . DIRECTORY_SEPARATOR;
            foreach (scandir($dir) as $photo) {
                if(Yii::app()->controller->life) Yii::app()->controller->result('progress', false);
                if (is_file($dir . $photo) && !in_array($photo, $photos))
                    @unlink($dir . $photo);
            }
        }
    }
}